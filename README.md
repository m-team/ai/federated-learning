
Federated learning frameworks:
 1. TensorFlow Federated (TFF): TensorFlow Federated is an open-source framework built on top of TensorFlow that allows for federated learning experiments. It provides high-level abstractions and APIs for defining federated learning algorithms. Documentation:  https://www.tensorflow.org/federated

 2. PySyft: is a Python library for secure, private Deep Learning. PySyft decouples private data from model training, using Federated Learning, Differential Privacy, and Multi-Party Computation (MPC) within PyTorch. https://github.com/OpenMined/PySyft

 3. Flower: Flower is a federated learning framework that supports multiple machine learning frameworks such as PyTorch, TensorFlow, and Keras. It provides a high-level API for federated learning experiments and supports various configurations and customization options. Documentation: https://flower.dev/docs/tutorial/Flower-0-What-is-FL.html

 4. nvflare: NVIDIA FLARE (NVIDIA Federated Learning Application Runtime Environment) is a domain-agnostic, open-source, extensible SDK that allows researchers and data scientists to adaptexisting ML/DL workflows (PyTorch, RAPIDS, Nemo, TensorFlow) to a federated paradigm; and enables platform developers to build a secure, privacy preserving offering for a distributed multi-party collaboration:
  related links for nvflare:

   Documentation: https://nvflare.readthedocs.io/en/2.3.0/

   Github repository:   https://github.com/NVIDIA/NVFlare.git
   
   youtube Vídeos: https://www.youtube.com/watch?v=Q753J0qsJ8Q
 
 nvflare modes:
 1. FL Simulator mode:  is lightweight and uses threads to simulate different clients. For a quick research run, use the FL Simulator
 2.  POC mode:  is an insecure deployment run locally on one machine without worry about TLS certificates. For simulating real cases within the same machine, use POC or production
 3. Production mode: is secure with TLS certificates.For all other cases, use production mode.

  Custom FL applications can contain the folders:

        1. custom: contains any custom components (custom Python code)

        2. config: contains client and server configurations (config_fed_client.json, config_fed_server.json)

        3. resources: contains the logger config (log.config)


 
