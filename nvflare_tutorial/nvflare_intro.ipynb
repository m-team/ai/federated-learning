{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# NVFlare\n",
    "-  [NVFLARE](https://nvflare.readthedocs.io/en/main/index.html) (NVIDIA Federated Learning Application Runtime Environment) is a domain-agnostic, open-source, extensible SDK that allows researchers and data scientists to adapt existing ML/DL workflows(PyTorch, TensorFlow, Scikit-learn, XGBoost etc.) to a federated paradigm. \n",
    "\n",
    "<div style=\"text-align:center\">\n",
    "    <figure>\n",
    "        <img src=\"https://git.scc.kit.edu/m-team/ai/federated-learning/raw/main/FL_stack.jpg\" alt=\"alt text\"/>\n",
    "        <figcaption>The high-level NVIDIA FLARE architecture.</figcaption>\n",
    "    </figure>\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- A runtime environment enabling data scientists and researchers to  carry out FL experiments in a real-world scenario. \n",
    "\n",
    "- Nvidia FLARE supports multiple task execution, maximizing data scientist’s productivity.\n",
    "\n",
    "- System capabilities to start up federated learning with high availability infrastructure.\n",
    "\n",
    "- Built-in implementations of:\n",
    "\n",
    "    - Federated training workflows (scatter-and-gather, Cyclic)\n",
    "\n",
    "    - Federated evaluation workflows (global model evaluation, cross site model validation);\n",
    "\n",
    "    - Learning algorithms (FedAvg, FedOpt, FedProx)\n",
    "\n",
    "    - Privacy preserving algorithms (homomorphic encryption, differential privacy)\n",
    "\n",
    "- Extensible management tools for:\n",
    "\n",
    "    - Secure provisioning (TLS certificates)\n",
    "\n",
    "    - Orchestration (Admin Console) | (Admin APIs)\n",
    "\n",
    "    - Monitoring of federated learning experiments (Aux APIs; Tensorboard visualization)\n",
    "\n",
    "- A rich set of programmable APIs allowing researchers to create new federated workflows, learning & privacy preserving algorithms.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Privacy Preservation and Security Managment\n",
    "\n",
    "- NVFLARE implements security measures in the following areas:\n",
    "\n",
    "    - [Identity Security](https://nvflare.readthedocs.io/en/2.2.1/real_world_fl/overview.html#provision-start-operate): the authentication and authorization of communicating parties\n",
    "\n",
    "    - [Communication Security](https://nvflare.readthedocs.io/en/2.2.1/user_guide/nvflare_security.html#communication-security): the confidentiality of data communication messages.\n",
    "\n",
    "    - [Message Serialization](https://nvflare.readthedocs.io/en/2.2.1/programming_guide/serialization.html): techniques for ensuring safe serialization/deserialization process between communicating parties\n",
    "\n",
    "    - [Data Privacy Protection](https://nvflare.readthedocs.io/en/2.2.1/user_guide/nvflare_security.html#data-privacy-protection): techniques for preventing local data from being leaked and/or reverse-engineered.\n",
    "\n",
    "    - [Auditing](https://nvflare.readthedocs.io/en/2.2.1/user_guide/nvflare_security.html#enhanced-auditing): techniques for keep audit trails of critical events (e.g. commands issued by users, learning/training related events that can be analyzed to understand the final results)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Authentication](https://nvflare.readthedocs.io/en/2.2.1/user_guide/nvflare_security.html#authentication)\n",
    "- NVFLARE’s authentication model is based on Public Key Infrastructure (PKI) technology:\n",
    "\n",
    "- For the FL project, the Project Admin uses the Provisioning Tool to create a Root CA with a self-signed root certificate. This Root CA will be used to issue all other certs needed by communicating parties.\n",
    "\n",
    "- Identities involved in the study (Server(s), Clients, the Overseer, Users) are provisioned with the Provisioning Tool.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#  [Authorization](https://nvflare.readthedocs.io/en/2.2.1/real_world_fl/authorization.html#authorization)\n",
    "- Organization can define strict policy to control access to their computing resources and/or FL jobs. for example: \n",
    "    - Restrict custom code to be part of a study to only the org’s own researchers;\n",
    "\n",
    "    - Allow jobs only from its own researchers, or from specified other orgs, or even from specified trusted other researchers;\n",
    "\n",
    "    - Totally disable remote shell commands on its sites\n",
    "\n",
    "    - Allow the “ls” shell command but disable all other remote shell commands\n",
    "- This locally defined policy is loaded by FL Clients owned by the organization\n",
    "- The policy is also enforced by these FL Clients\n",
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# High Availability and Server Failover (HA)\n",
    "\n",
    "- Previously in NVIDIA FLARE 2.0 and before, the FL server was the single point of failure for the system.\n",
    "\n",
    "- Starting with NVIDIA FLARE 2.1.0, a [high availability](https://nvflare.readthedocs.io/en/2.2.1/programming_guide/high_availability.html#high-availability-and-server-failover )(HA) solution has been implemented to support multiple FL servers with automatic cutover when the currently active server becomes unavailable.\n",
    "\n",
    "    - There can now be any number of FL servers  \n",
    "\n",
    "    - A new service called Overseer was added to oversee the overall availability of its clients  \n",
    "\n",
    "    - Overseer Agents were added the clients of the Overseer service (FL servers, FL clients, and admins) to constantly communicate with the Overseer to report its status and to get the current system status.\n",
    "    \n",
    "\n",
    "    <div style=\"text-align:center\">\n",
    "    <figure>\n",
    "        <img src=\"https://git.scc.kit.edu/m-team/ai/federated-learning/raw/main/high-availability-featured.png\" alt=\"alt text\"/>\n",
    "        <figcaption>The NVIDIA FLARE deployment for high availability (HA).</figcaption>\n",
    "    </figure>\n",
    "</div>\n",
    "\n",
    " \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Federated statistics\n",
    "\n",
    "- When working with distributed datasets, it is often important to assess the data quality and distribution across the set of client datasets. \n",
    "\n",
    "- FLARE 2.2 provides a set of [federated statistics](https://github.com/NVIDIA/NVFlare/tree/main/examples/advanced/federated-statistics) operators ([controllers](https://nvflare.readthedocs.io/en/main/programming_guide/controllers.html) and [executors](https://nvflare.readthedocs.io/en/main/programming_guide/executor.html)) that can be used to generate global statistics based on individual client-side statistics.  \n",
    "\n",
    "- The workflow controller and executor are designed to allow data scientists to quickly implement their own statistical methods (generators) based on the specifics of their datasets of interest. \n",
    "\n",
    "- Commonly used statistics are provided out-of-the box, including count, sum, mean, standard deviation, and histograms, along with routines to visualize the global and individual statistics. \n",
    "\n",
    "- The max, min are not included as it might violate the client's data privacy. Median is not included due to the complexity of the algorithms. \n",
    "\n",
    "- The built-in visualization tools can be used to view statistics across all datasets at all sites as well as global aggregates\n",
    "\n",
    "<div style=\"text-align:center\">\n",
    "    <figure>\n",
    "        <img src=\"https://git.scc.kit.edu/m-team/ai/federated-learning/raw/main/image1-6.png\" alt=\"alt text\"/>\n",
    "        <figcaption>  Example histograms from the federated statistics image example using the built-in FLARE statistics visualization class</figcaption>\n",
    "    </figure>\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Other features of the NVFlare\n",
    "\n",
    "- [NVFLARE Dashboard UI](https://nvflare.readthedocs.io/en/2.2.1/user_guide/dashboard_ui.html): The NVFlare Dashboard is a new optional addition to NVIDIA FLARE in version 2.2 that allows for the project administrator to deploy a website to gather information about the sites and distribute startup kits.\n",
    "\n",
    "- [NVIDIA FLARE Preflight Check](https://nvflare.readthedocs.io/en/2.2.1/user_guide/preflight_check.html): The NVIDIA FLARE preflight check was added in version 2.2 to help perform preliminary checks before users start an NVFlare subsystem on their machine to catch errors early and mitigate the pain of setting up and running jobs in NVIDIA FLARE.\n",
    "\n",
    "- [Launching NVIDIA FLARE with docker compose](https://nvflare.readthedocs.io/en/2.2.1/user_guide/docker_compose.html): For users who would like to get NVIDIA FLARE up and running as easy as possible, such as first-time NVIDIA FLARE users or people who need to demonstrate it upon request, they can use this docker compose feature. All they need is a working docker environment.\n",
    "\n",
    "- [Helm Chart for NVIDIA FLARE](https://nvflare.readthedocs.io/en/2.2.1/user_guide/helm_chart.html) to deploy NVIDIA FLARE to an existing Kubernetes cluster\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# NVIDA MODES\n",
    "- FL simulator\n",
    "- POC mode\n",
    "- Production mode "
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
